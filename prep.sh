#!/bin/bash

set -eu

# set up dependent packages

export DEBIAN_FRONTEND=noninteractive

# This is the build tool-chain we use on the launchpad
# build servers. We don't do a build here, but we should use
# the same version of cargo when vendoring dependencies to
# avoid any surprises.
if [ "$DISTRO" = "focal" ] ; then
  # for some distros we need a newer version of rust
  apt-get update
  apt-get install -y software-properties-common
  add-apt-repository ppa:ubuntu-mozilla-security/rust-updates
fi
apt-get update

apt-get install -y \
    curl \
    unzip \
    jq \
    git \
    devscripts \
    rustc \
    cargo \
    cmake \
    debhelper \
    help2man \
    libfreetype6-dev \
    libfontconfig1-dev \
    libxcb1-dev \
    libxkbcommon-dev \
    libxcb-render0-dev \
    libxcb-shape0-dev \
    libxcb-xfixes0-dev

# install the aws-cli
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.17.14.zip" -o "awscliv2.zip"
sha256sum awscliv2.zip
echo "486334adf064c8ca2b1e73478032eb3f85b44605a710d2fbeefdaa01e77f958e  awscliv2.zip" | sha256sum --check
unzip -q awscliv2.zip
./aws/install
rm -rf ./aws*

# remove utilities we're not using
apt-get remove -y curl unzip

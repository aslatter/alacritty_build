#!/bin/bash

set -eu

# set up environment

if [[ -n ${GPG_KEY:-} && -n ${GPG_KEY_ID:-} ]]; then
    gpg --import "$GPG_KEY"
    echo -e "5\ny\n" | gpg --batch --command-fd 0 --edit-key "$GPG_KEY_ID" trust
fi

#!/bin/bash

set -euo pipefail

# Set the last-built revision for a distro

# $1 - distribution
# $2 - revision

log_err() {
  echo >&2 "$1"
}

EXPECTED_ARG_COUNT=2
if [[ $# -ne $EXPECTED_ARG_COUNT ]]; then
  log_err "Invalid number of arguments: $#; expected $EXPECTED_ARG_COUNT"
  exit 1
fi

DISTRO="$1"
GIT_HASH="$2"

DB_KEY="alacritty::$DISTRO::last_built"

DOCUMENT="{\"id\":{\"S\":\"$DB_KEY\"},\"val\":{\"S\":\"$GIT_HASH\"}}"

aws dynamodb put-item --table-name alacritty-ppa --item "$DOCUMENT"

echo "Logged revision ${GIT_HASH} built for ${DISTRO}."

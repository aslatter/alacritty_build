#!/bin/bash

set -eu

repo_root=$PWD

# check out alacritty master
mkdir -p working
cd working
git clone https://github.com/alacritty/alacritty.git alacritty
cd alacritty

git_hash=$(git rev-parse HEAD)

set +e
[[ -n "${AWS_DEFAULT_REGION:-}" && -n "${DISTRO:-}" ]]
# shellcheck disable=SC2319
should_check_db=$?
set -e

if [[ "$should_check_db" == "0" ]]; then
    last_built=$("$repo_root/scripts/get_built.sh" "$DISTRO")
    if [[ "$last_built" == "$git_hash" ]]; then
        echo "Revision $(git rev-parse --short HEAD) already built for ${DISTRO}. Skipping."
        exit 0
    fi
else
    echo "Not checking db ..."
fi

# Create version string. Must be 'greater than'
# most recent real release, but 'less than' the
# next real release. We also want to include
# useful information like the date and current
# git commit.

# To fit this critera, we base our version number
# on the most-recent non-RC tag, and add some stuff
# after the end of it. The PPA docs recommend '+1'
# followed by whatever information we want.

# Find 'most recent' non-rc tag.
# I'm bad at shell so this is kinda dumb.
# This is assuming that the highest-versioned
# tag is somehow most relevant to master.
found_version=""
for tag in $(git tag | sort -V); do
    # looks line a non-rc version?
    if [[ $tag =~ ^v([0-9]+\.)*[0-9]+$ ]]; then
        # trim the leading 'v'
        found_version="${tag:1}"
    fi
done

git_short_hash=$(git rev-parse --short HEAD)
date_str=$(date -u +"%Y%m%dT%H%M%S")

short_version="$found_version+1-$date_str${DISTRO:+~$DISTRO}"
version="$short_version-$git_short_hash"

# add on pop_os debianization
git remote add pop https://github.com/pop-os/alacritty.git
git fetch pop
git restore --source="pop/${POP_OS_SOURCE_BRANCH}" debian

# edit the debian/changelog for the new version
tmp_changelog=$(mktemp)
{
    echo "alacritty ($version) ${DISTRO:-UNRELEASED}; urgency=low"
    echo
    echo "  * Packaged from git master"
    echo
    echo " -- ${AUTHOR:-Unknown Author}  $(date -R)"
    echo
} > "$tmp_changelog"

cat debian/changelog >> "$tmp_changelog"
mv "$tmp_changelog" debian/changelog

# make orig tarball

git archive --format=tar HEAD | xz > "../alacritty_$short_version.orig.tar.xz"

# gross hacks until Cargo.lock for ndk-sys goes above 0.4.0
# https://github.com/rust-windowing/android-ndk-rs/issues/369
cargo update -p ndk-sys

# build!
debuild -S

# deb file is in parent folder
if [[ -n ${DISTRO:-} ]]; then
    pushd ..
    dput "$UPLOAD_HOST" ./*.changes
    popd
fi

if [[ "$should_check_db" == "0" ]]; then
    "$repo_root/scripts/set_built.sh" "$DISTRO" "$git_hash"
else
    echo "Not updating db ..."
fi

This project scripts the debian-ization of the 'Alacritty' terminal
emulator from git-master. I don't really know Debian packaging so
it's a bit hacked together.

All credit goes to the Alacritty developers and the Pop!_os
maintainers for doing the actual work.

The build-script (./build.sh) performs the following:

+ Checks out Alacritty master
+ Copies the Debian packaging from [Pop!_os][0]
+ Builds the Debian package
+ Uploads the source-package to a PPA

Additionally, we track the last-built git-SHA per Ubuntu
distro in an AWS DynamoDB table, to avoid doing builds
which aren't needed.

# Overview

+ *prep.sh* - installs required dependencies into the environment
+ *setup.sh* - sets up required environment settings (mostly the OpenPGP signing key)
+ *build.sh* - check out alacritty source, build and upload the source package

 [0]: https://github.com/pop-os/alacritty.git
